<?php

namespace Aberkow;

class Hello_World {
  public function __construct() {
    $this->message = "Hello world";
  }
  public function init() {
    return $this->message;
  }
}